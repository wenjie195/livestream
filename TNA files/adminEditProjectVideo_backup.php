<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Platform.php';
require_once dirname(__FILE__) . '/classes/Subshare.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$platformDetails = getPlatform($conn," WHERE status = 'Available' AND type = '1' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Project Video | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>Edit Project Video  | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="光明線上產業展, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <h2 class="h1-title">Edit Project Video</h2>

    <div class="clear"></div>

    <?php
    if(isset($_POST['subdata_uid']))
    {
        $conn = connDB();
        $subDetails = getSubShare($conn,"WHERE uid = ? ", array("uid") ,array($_POST['subdata_uid']),"s");
    ?>

        <!-- <form action="utilities/editSubFunction.php" method="POST" enctype="multipart/form-data"> -->
        <form action="utilities/editSubFunction.php" method="POST" enctype="multipart/form-data"> 

            <h4 class="margin-top30"><b>Project Video 1</b></h4>  
                            
            <div class="dual-input">
                <p class="input-top-text">Project Title 1</p>
                <input class="aidex-input clean" type="text" value="<?php echo $subDetails[0]->getTitle();?>" name="update_title_one" id="update_title_one">       
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text">File 1 : <a href="uploadsFiles/<?php echo $subDetails[0]->getFile();?>" class="blue-to-orange" target="_blank"><?php echo $subDetails[0]->getFile();?></a></p>
                <p><input id="file-upload" type="file" name="file_one" id="file_one" class="margin-bottom10 pointer" /></p>
                <input class="aidex-input clean" type="hidden" value="<?php echo $subDetails[0]->getFile();?>" name="ori_file_one" id="ori_file_one">       
            </div>

            <div class="dual-input">
                <p class="input-top-text">Host One</p>
                <input class="aidex-input clean" type="text" value="<?php echo $subDetails[0]->getHost();?>" name="update_host" id="update_host">       
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text">Platform One</p>

                <select class="aidex-input clean" type="text" name="update_platform" id="update_platform">
                    <option value="">Please Select A Platform</option>
                    <?php
                    if($subDetails[0]->getPlatform() == '')
                    {
                    ?>
                        <option selected>Please Select a Platform</option>
                        <?php
                        for ($cnt=0; $cnt <count($platformDetails) ; $cnt++)
                        {
                        ?>
                            <option value="<?php echo $platformDetails[$cnt]->getPlatformType(); ?>"> 
                                <?php echo $platformDetails[$cnt]->getPlatformType(); ?>
                            </option>
                        <?php
                        }
                    }
                    else
                    {
                        for ($cnt=0; $cnt <count($platformDetails) ; $cnt++){
                            if ($subDetails[0]->getPlatform() == $platformDetails[$cnt]->getPlatformType())
                            {
                            ?>
                                <option selected value="<?php echo $platformDetails[$cnt]->getPlatformType(); ?>"> 
                                    <?php echo $platformDetails[$cnt]->getPlatformType(); ?>
                                </option>
                            <?php
                            }
                            else
                            {
                            ?>
                                <option value="<?php echo $platformDetails[$cnt]->getPlatformType(); ?>"> 
                                    <?php echo $platformDetails[$cnt]->getPlatformType(); ?>
                                </option>
                            <?php
                            }
                        }
                    }
                    ?>
                </select> 

            </div>
            <div class="clear"></div>
            
            <div class="dual-input">
                <p class="input-top-text">Link One</p>
                <input class="aidex-input clean" type="text" value="<?php echo $subDetails[0]->getLink();?>" name="update_link" id="update_link">       
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text">Remark One</p>
                <input class="aidex-input clean" type="text" value="<?php echo $subDetails[0]->getRemark();?>" name="update_remark" id="update_remark">       
            </div>

            <div class="clear"></div>  

            <h4 class="margin-top30"><b>Project Video 2</b></h4>

            <div class="dual-input">
                <p class="input-top-text">Project Title 2</p>
                <input class="aidex-input clean" type="text" value="<?php echo $subDetails[0]->getTitleTwo();?>" name="update_title_two" id="update_title_two">       
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text">File 2 : <a href="uploadsFiles/<?php echo $subDetails[0]->getFileTwo();?>" class="blue-to-orange" target="_blank"><?php echo $subDetails[0]->getFileTwo();?></a></p>
                <p><input id="file-upload" type="file" name="file_two" id="file_two" class="margin-bottom10 pointer" /></p>  
                <input class="aidex-input clean" type="hidden" value="<?php echo $subDetails[0]->getFileTwo();?>" name="ori_filetwo" id="ori_filetwo">       
            </div>

            <div class="clear"></div>  

            <div class="dual-input">
                <p class="input-top-text">Host Two</p>
                <input class="aidex-input clean" type="text" value="<?php echo $subDetails[0]->getHostTwo();?>" name="update_host_two" id="update_host_two">       
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text">Platform Two</p>                    
                <select class="aidex-input clean" type="text" name="update_platform_two" id="update_platform_two">
                    <?php
                    if($subDetails[0]->getPlatformTwo() == '')
                    {
                    ?>
                        <option selected>Please Select a Platform</option>
                        <?php
                        for ($cntAA=0; $cntAA <count($platformDetails) ; $cntAA++)
                        {
                        ?>
                            <option value="<?php echo $platformDetails[$cntAA]->getPlatformType(); ?>"> 
                                <?php echo $platformDetails[$cntAA]->getPlatformType(); ?>
                            </option>
                        <?php
                        }
                    }
                    else
                    {
                        for ($cntAA=0; $cntAA <count($platformDetails) ; $cntAA++){
                            if ($subDetails[0]->getPlatformTwo() == $platformDetails[$cntAA]->getPlatformType())
                            {
                            ?>
                                <option selected value="<?php echo $platformDetails[$cntAA]->getPlatformType(); ?>"> 
                                    <?php echo $platformDetails[$cntAA]->getPlatformType(); ?>
                                </option>
                            <?php
                            }
                            else
                            {
                            ?>
                                <option value="<?php echo $platformDetails[$cntAA]->getPlatformType(); ?>"> 
                                    <?php echo $platformDetails[$cntAA]->getPlatformType(); ?>
                                </option>
                            <?php
                            }
                        }
                    }
                    ?>

                </select> 

            </div>
            <div class="clear"></div>  
            <div class="dual-input">
                <p class="input-top-text">Link Two</p>
                <input class="aidex-input clean" type="text" value="<?php echo $subDetails[0]->getLinkTwo();?>" name="update_link_two" id="update_link_two">       
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-text">Remark Two</p>
                <input class="aidex-input clean" type="text" value="<?php echo $subDetails[0]->getRemarkTwo();?>" name="update_remark_two" id="update_remark_two">       
            </div>

            <div class="clear"></div>  
            
            <input type="hidden" value="<?php echo $subDetails[0]->getUid();?>" name="sub_uid" id="sub_uid" readonly> 

            <div class="clear"></div>  

            <div class="width100 overflow text-center">     
                <button class="clean-button clean login-btn pink-button" name="submit">Submit</button>
            </div>

        </form>

    <?php
    }
    ?>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>