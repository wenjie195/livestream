<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Image.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Project Logo and Link | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>Project Logo and Link | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="光明線上產業展, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'adminHeader.php'; ?>

<div class="width100 same-padding overflow gold-bg min-height-footer-only">

    <h2 class="h1-title">Project Logo and Link</h2> 
	<div class="project-logo-big-div overflow">
    	<!-- php repeat here --->
        <?php
        if(isset($_POST['user_uid']))
        $conn = connDB();
        $userImage = getImage($conn,"WHERE user_uid = ? ", array("user_uid") ,array($_POST['user_uid']),"s");
        if($userImage)
        {
            for($cnt = 0;$cnt < count($userImage) ;$cnt++)
            {
            ?>        
    	<div class="project-logo-repeat-div">
                        <form action="adminUpdateProjectLogo.php" method="POST" class="hover1" target="_blank">
                            <input class="aidex-input clean"  type="hidden" value="<?php echo $_POST['user_uid'];?>" id="user_uid" name="user_uid" readonly>
                            <input class="aidex-input clean"  type="hidden" value="<?php echo $userImage[$cnt]->getUid();?>" id="pic_link_uid" name="pic_link_uid" readonly>
                            <button class="clean edit-project-logo-btn view-project-logo hover1" name="submit"><img src="uploads/<?php echo $userImage[$cnt]->getImageOne();?>" class="view-project-logo hover1a">
                            <img src="img/change-project-logo.png" alt="Update Project Logo" title="Update Project Logo" class="view-project-logo hover1b"></button>
                            
                        </form>    
                        
                
                <form action="adminUpdateProjectLink.php" method="POST" class="hover1" target="_blank">
                    <input class="aidex-input clean"  type="hidden" value="<?php echo $_POST['user_uid'];?>" id="user_uid" name="user_uid" readonly>
                    <input class="aidex-input clean"  type="hidden" value="<?php echo $userImage[$cnt]->getUid();?>" id="pic_link_uid" name="pic_link_uid" readonly><p class="text-overflow link-preview"><button class="clean update-btn hover1" name="submit"><img src="img/edit.png" class="edit-btn-img hover1a"><img src="img/edit2.png" class="edit-btn-img hover1b"> https://example.com/1213124125151251251515</button></p>     	
                    
                </form>
                <button class="clean-button clean login-btn pink-button" name="">Delete</button>         
        </div>
        
        
    	<!-- End of php repeat -->
        <!-- Delete all -->
    	<div class="project-logo-repeat-div">
                        <form action="adminUpdateProjectLogo.php" method="POST" class="hover1" target="_blank">
                            <input class="aidex-input clean"  type="hidden" value="<?php echo $_POST['user_uid'];?>" id="user_uid" name="user_uid" readonly>
                            <input class="aidex-input clean"  type="hidden" value="<?php echo $userImage[$cnt]->getUid();?>" id="pic_link_uid" name="pic_link_uid" readonly>
                            <button class="clean edit-project-logo-btn view-project-logo hover1" name="submit"><img src="uploads/<?php echo $userImage[$cnt]->getImageOne();?>" class="view-project-logo hover1a">
                            <img src="img/change-project-logo.png" alt="Update Project Logo" title="Update Project Logo" class="view-project-logo hover1b"></button>
                            
                        </form>    
                        
                
                <form action="adminUpdateProjectLink.php" method="POST" class="hover1" target="_blank">
                    <input class="aidex-input clean"  type="hidden" value="<?php echo $_POST['user_uid'];?>" id="user_uid" name="user_uid" readonly>
                    <input class="aidex-input clean"  type="hidden" value="<?php echo $userImage[$cnt]->getUid();?>" id="pic_link_uid" name="pic_link_uid" readonly><p class="text-overflow link-preview"><button class="clean update-btn hover1" name="submit"><img src="img/edit.png" class="edit-btn-img hover1a"><img src="img/edit2.png" class="edit-btn-img hover1b"> https://example.com/1213124125151251251515</button></p>     	
                    
                </form>
                <button class="clean-button clean login-btn pink-button" name="">Delete</button>         
        </div>
    	<div class="project-logo-repeat-div">
                        <form action="adminUpdateProjectLogo.php" method="POST" class="hover1" target="_blank">
                            <input class="aidex-input clean"  type="hidden" value="<?php echo $_POST['user_uid'];?>" id="user_uid" name="user_uid" readonly>
                            <input class="aidex-input clean"  type="hidden" value="<?php echo $userImage[$cnt]->getUid();?>" id="pic_link_uid" name="pic_link_uid" readonly>
                            <button class="clean edit-project-logo-btn view-project-logo hover1" name="submit"><img src="uploads/<?php echo $userImage[$cnt]->getImageOne();?>" class="view-project-logo hover1a">
                            <img src="img/change-project-logo.png" alt="Update Project Logo" title="Update Project Logo" class="view-project-logo hover1b"></button>
                            
                        </form>    
                        
                
                <form action="adminUpdateProjectLink.php" method="POST" class="hover1" target="_blank">
                    <input class="aidex-input clean"  type="hidden" value="<?php echo $_POST['user_uid'];?>" id="user_uid" name="user_uid" readonly>
                    <input class="aidex-input clean"  type="hidden" value="<?php echo $userImage[$cnt]->getUid();?>" id="pic_link_uid" name="pic_link_uid" readonly><p class="text-overflow link-preview"><button class="clean update-btn hover1" name="submit"><img src="img/edit.png" class="edit-btn-img hover1a"><img src="img/edit2.png" class="edit-btn-img hover1b"> https://example.com/1213124125151251251515</button></p>     	
                    
                </form>
                <button class="clean-button clean login-btn pink-button" name="">Delete</button>         
        </div>
    	<div class="project-logo-repeat-div">
                        <form action="adminUpdateProjectLogo.php" method="POST" class="hover1" target="_blank">
                            <input class="aidex-input clean"  type="hidden" value="<?php echo $_POST['user_uid'];?>" id="user_uid" name="user_uid" readonly>
                            <input class="aidex-input clean"  type="hidden" value="<?php echo $userImage[$cnt]->getUid();?>" id="pic_link_uid" name="pic_link_uid" readonly>
                            <button class="clean edit-project-logo-btn view-project-logo hover1" name="submit"><img src="uploads/<?php echo $userImage[$cnt]->getImageOne();?>" class="view-project-logo hover1a">
                            <img src="img/change-project-logo.png" alt="Update Project Logo" title="Update Project Logo" class="view-project-logo hover1b"></button>
                            
                        </form>    
                        
                
                <form action="adminUpdateProjectLogo.php" method="POST" class="hover1" target="_blank">
                    <input class="aidex-input clean"  type="hidden" value="<?php echo $_POST['user_uid'];?>" id="user_uid" name="user_uid" readonly>
                    <input class="aidex-input clean"  type="hidden" value="<?php echo $userImage[$cnt]->getUid();?>" id="pic_link_uid" name="pic_link_uid" readonly><p class="text-overflow link-preview"><button class="clean update-btn hover1" name="submit"><img src="img/edit.png" class="edit-btn-img hover1a"><img src="img/edit2.png" class="edit-btn-img hover1b"> https://example.com/1213124125151251251515</button></p>     	
                    
                </form>
                <button class="clean-button clean login-btn pink-button" name="">Delete</button>         
        </div>
    	<div class="project-logo-repeat-div">
                        <form action="adminReviewUserLogo.php" method="POST" class="hover1" target="_blank">
                            <input class="aidex-input clean"  type="hidden" value="<?php echo $_POST['user_uid'];?>" id="user_uid" name="user_uid" readonly>
                            <input class="aidex-input clean"  type="hidden" value="<?php echo $userImage[$cnt]->getUid();?>" id="pic_link_uid" name="pic_link_uid" readonly>
                            <button class="clean edit-project-logo-btn view-project-logo hover1" name="submit"><img src="uploads/<?php echo $userImage[$cnt]->getImageOne();?>" class="view-project-logo hover1a">
                            <img src="img/change-project-logo.png" alt="Update Project Logo" title="Update Project Logo" class="view-project-logo hover1b"></button>
                            
                        </form>    
                        
                
                <form action="adminUpdateProjectLink.php" method="POST" class="hover1" target="_blank">
                    <input class="aidex-input clean"  type="hidden" value="<?php echo $_POST['user_uid'];?>" id="user_uid" name="user_uid" readonly>
                    <input class="aidex-input clean"  type="hidden" value="<?php echo $userImage[$cnt]->getUid();?>" id="pic_link_uid" name="pic_link_uid" readonly><p class="text-overflow link-preview"><button class="clean update-btn hover1" name="submit"><img src="img/edit.png" class="edit-btn-img hover1a"><img src="img/edit2.png" class="edit-btn-img hover1b"> https://example.com/1213124125151251251515</button></p>     	
                    
                </form>
                <button class="clean-button clean login-btn pink-button" name="">Delete</button>         
        </div>
    	<div class="project-logo-repeat-div">
                        <form action="adminUpdateProjectLogo.php" method="POST" class="hover1" target="_blank">
                            <input class="aidex-input clean"  type="hidden" value="<?php echo $_POST['user_uid'];?>" id="user_uid" name="user_uid" readonly>
                            <input class="aidex-input clean"  type="hidden" value="<?php echo $userImage[$cnt]->getUid();?>" id="pic_link_uid" name="pic_link_uid" readonly>
                            <button class="clean edit-project-logo-btn view-project-logo hover1" name="submit"><img src="uploads/<?php echo $userImage[$cnt]->getImageOne();?>" class="view-project-logo hover1a">
                            <img src="img/change-project-logo.png" alt="Update Project Logo" title="Update Project Logo" class="view-project-logo hover1b"></button>
                            
                        </form>    
                        
                
                <form action="adminUpdateProjectLink.php" method="POST" class="hover1" target="_blank">
                    <input class="aidex-input clean"  type="hidden" value="<?php echo $_POST['user_uid'];?>" id="user_uid" name="user_uid" readonly>
                    <input class="aidex-input clean"  type="hidden" value="<?php echo $userImage[$cnt]->getUid();?>" id="pic_link_uid" name="pic_link_uid" readonly><p class="text-overflow link-preview"><button class="clean update-btn hover1" name="submit"><img src="img/edit.png" class="edit-btn-img hover1a"><img src="img/edit2.png" class="edit-btn-img hover1b"> https://example.com/1213124125151251251515</button></p>     	
                    
                </form>
                <button class="clean-button clean login-btn pink-button" name="">Delete</button>         
        </div>
    	<div class="project-logo-repeat-div">
                        <form action="adminUpdateProjectLogo.php" method="POST" class="hover1" target="_blank">
                            <input class="aidex-input clean"  type="hidden" value="<?php echo $_POST['user_uid'];?>" id="user_uid" name="user_uid" readonly>
                            <input class="aidex-input clean"  type="hidden" value="<?php echo $userImage[$cnt]->getUid();?>" id="pic_link_uid" name="pic_link_uid" readonly>
                            <button class="clean edit-project-logo-btn view-project-logo hover1" name="submit"><img src="uploads/<?php echo $userImage[$cnt]->getImageOne();?>" class="view-project-logo hover1a">
                            <img src="img/change-project-logo.png" alt="Update Project Logo" title="Update Project Logo" class="view-project-logo hover1b"></button>
                            
                        </form>    
                        
                
                <form action="adminUpdateProjectLink.php" method="POST" class="hover1" target="_blank">
                    <input class="aidex-input clean"  type="hidden" value="<?php echo $_POST['user_uid'];?>" id="user_uid" name="user_uid" readonly>
                    <input class="aidex-input clean"  type="hidden" value="<?php echo $userImage[$cnt]->getUid();?>" id="pic_link_uid" name="pic_link_uid" readonly><p class="text-overflow link-preview"><button class="clean update-btn hover1" name="submit"><img src="img/edit.png" class="edit-btn-img hover1a"><img src="img/edit2.png" class="edit-btn-img hover1b"> https://example.com/1213124125151251251515</button></p>     	
                    
                </form>
                <button class="clean-button clean login-btn pink-button" name="">Delete</button>         
        </div>
    	<div class="project-logo-repeat-div">
                        <form action="adminUpdateProjectLogo.php" method="POST" class="hover1" target="_blank">
                            <input class="aidex-input clean"  type="hidden" value="<?php echo $_POST['user_uid'];?>" id="user_uid" name="user_uid" readonly>
                            <input class="aidex-input clean"  type="hidden" value="<?php echo $userImage[$cnt]->getUid();?>" id="pic_link_uid" name="pic_link_uid" readonly>
                            <button class="clean edit-project-logo-btn view-project-logo hover1" name="submit"><img src="uploads/<?php echo $userImage[$cnt]->getImageOne();?>" class="view-project-logo hover1a">
                            <img src="img/change-project-logo.png" alt="Update Project Logo" title="Update Project Logo" class="view-project-logo hover1b"></button>
                            
                        </form>    
                        
                
                <form action="adminUpdateProjectLink.php" method="POST" class="hover1" target="_blank">
                    <input class="aidex-input clean"  type="hidden" value="<?php echo $_POST['user_uid'];?>" id="user_uid" name="user_uid" readonly>
                    <input class="aidex-input clean"  type="hidden" value="<?php echo $userImage[$cnt]->getUid();?>" id="pic_link_uid" name="pic_link_uid" readonly><p class="text-overflow link-preview"><button class="clean update-btn hover1" name="submit"><img src="img/edit.png" class="edit-btn-img hover1a"><img src="img/edit2.png" class="edit-btn-img hover1b"> https://example.com/1213124125151251251515</button></p>     	
                    
                </form>
                <button class="clean-button clean login-btn pink-button" name="">Delete</button>         
        </div>                                                
        <!-- End of Delete -->
    
    
    </div>    


<!--
               
                    <div class="four-div overflow">

                    </div>
              
                    <div class="four-div overflow">
                        <form action="adminReviewUserLogoTwo.php" method="POST" class="hover1" target="_blank">
                            <input class="aidex-input clean"  type="hidden" value="<?php echo $_POST['user_uid'];?>" id="user_uid" name="user_uid" readonly>
                            <input class="aidex-input clean"  type="hidden" value="<?php echo $userImage[$cnt]->getUid();?>" id="pic_link_uid" name="pic_link_uid" readonly>

                            <?php 
                                $imgTwo = $userImage[$cnt]->getImageTwo();
                                if($imgTwo != "")
                                {
                                ?>
                                    <img src="uploads/<?php echo $userImage[$cnt]->getImageTwo();?>" class="first-logo project-logo opacity-hover">
                                <?php
                                }
                                else
                                {
                                ?>
                                    <img src=" " class="first-logo project-logo opacity-hover">
                                <?php
                                }
                            ?>

                            <button class="clean-button clean login-btn pink-button" name="submit">Update</button>
                        </form> 
       



            <?php
            }
            ?>
        <?php
        }
        ?>-->

    <div class="clear"></div>
    		<div class="width100 overflow text-center">
                <form method="POST" action="adminAddUserProjectLogo.php" class="hover1">
                    <button class="clean-button clean login-btn pink-button ow-mid-btn-width" type="submit" name="user_uid" value="<?php echo $_POST['user_uid'];?>">
                        Add Project Logo
                    </button>
            	</form>
            </div>
        <?php
        if(isset($_POST['user_uid']))
        $conn = connDB();
        $userImage = getImage($conn,"WHERE user_uid = ? ", array("user_uid") ,array($_POST['user_uid']),"s");
        if($userImage)
        {
            $totalImage = count($userImage);
        }
        else
        {   $totalImage = 0;   }
        ?>

        <?php
        if($totalImage < 4)
        {   
        ?>
    		<div class="width100 overflow text-center">
                <form method="POST" action="adminAddUserProjectLogo.php" class="hover1">
                    <button class="clean-button clean login-btn pink-button ow-mid-btn-width" type="submit" name="user_uid" value="<?php echo $_POST['user_uid'];?>">
                        Add Project Logo
                    </button>
            	</form>
            </div>
        <?php
        }
        else{}
        ?>

        <div class="clear"></div>       
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>