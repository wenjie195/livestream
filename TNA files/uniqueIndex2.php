<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Image.php';
require_once dirname(__FILE__) . '/classes/Liveshare.php';
require_once dirname(__FILE__) . '/classes/Platform.php';
require_once dirname(__FILE__) . '/classes/Subshare.php';
require_once dirname(__FILE__) . '/classes/Sharing.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $colorDetails = getColor($conn," WHERE type = ? ",array("type"),array(2),"s");
// $liveDetails = getLiveShare($conn," WHERE username = 'MahSing' AND status = 'Available' AND type = '1' ");
// $subDetails = getSubShare($conn," WHERE username = 'MahSing' AND status = 'Available' AND type = '1' ");

// $zoomDetails = getSubShare($conn," WHERE platform = 'Zoom' ");

// $subDetails = getSubShare($conn," WHERE status = 'Available' AND type = '1' ");

// $mainLive = getUser($conn," WHERE broadcast_live = 'Available' AND user_type = '1' ");
// $mainSub = getUser($conn," WHERE broadcast_share = 'Available' AND user_type = '1' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
    <?php
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
    $link = "https";
    else
    $link = "http";

    // Here append the common URL characters.
    $link .= "://";

    // Append the host(domain name, ip) to the URL.
    $link .= $_SERVER['HTTP_HOST'];

    // Append the requested resource location to the URL
    $link .= $_SERVER['REQUEST_URI'];


    if(isset($_GET['id']))
    {
        $referUidLink = $_GET['id'];
    }
    else
    {
        $referUidLink = "";
    }
    ?>

<?php
$conn = connDB();
$liveDetails = getLiveShare($conn,"WHERE user_uid = ? AND status = 'Available' ", array("user_uid") ,array($referUidLink),"s");
// $title = $liveDetails[0]->getUsername(); 
if($liveDetails)
{
for($cnt = 0;$cnt < count($liveDetails) ;$cnt++)
{
?>

<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/uniqueIndex2.php" />
<link rel="canonical" href="https://gmvec.com/uniqueIndex2.php" />
<meta property="og:title" content="<?php echo $liveDetails[$cnt]->getUsername();?> | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title><?php echo $liveDetails[$cnt]->getUsername();?> | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="<?php echo $liveDetails[$cnt]->getUsername();?>, 光明線上產業展, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live, etc">

<?php
}
?>
<?php
}
?>

<?php include 'css.php'; ?>
</head>

<body>

<div class="width100 gold-line"></div>

<div class="width100 same-padding overflow gold-bg min-height">

    <div class="width100 overflow margin-top30 first-div-margin">    
        
        <div class="width100 top-video-div overflow">
        <div class="left-video-div">
            <?php
            $conn = connDB();
            $liveDetails = getLiveShare($conn,"WHERE user_uid = ? AND status = 'Available' ", array("user_uid") ,array($referUidLink),"s");
            // $title = $liveDetails[0]->getUsername(); 
            if($liveDetails)
            {
                for($cnt = 0;$cnt < count($liveDetails) ;$cnt++)
                {
                ?>   
                    <?php 
                        $platfrom =  $liveDetails[$cnt]->getPlatform();
                        if($platfrom == 'Youtube')
                        {
                        ?>
                            
                                <iframe class="youtube-top-iframe" src="https://www.youtube.com/embed/<?php echo $liveDetails[$cnt]->getLink();?>?&playsinline=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            
                        <?php
                        }
                        elseif($platfrom == 'Facebook')
                        {
                        ?>
                            
                                <iframe class="youtube-top-iframe" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F?v=<?php echo $liveDetails[$cnt]->getLink();?>?&playsinline=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            
                        <?php
                        }
                        else
                        {   }
                    ?>
                <?php
                }
                ?>
            <?php
            }
            ?>
			</div>
            <div class="right-project-div">
            <?php
            if(isset($referUidLink))
            $conn = connDB();
            $projectLogo = getImage($conn,"WHERE user_uid = ? ", array("user_uid") ,array($referUidLink),"s");
            if($projectLogo)
            {
                for($cntAA = 0;$cntAA < count($projectLogo) ;$cntAA++)
                {
                ?>
                    
                        <a href="<?php echo $projectLogo[$cntAA]->getLinkOne();?>" target="_blank"><img src="uploads/<?php echo $projectLogo[$cntAA]->getImageOne();?>" class="project-logo opacity-hover"></a>
            
                        <?php 
                            $imgTwo = $projectLogo[$cntAA]->getImageTwo();
                            if($imgTwo != "")
                            {
                            ?>
                                <a href="<?php echo $projectLogo[$cntAA]->getLinkTwo();?>" target="_blank"><img src="uploads/<?php echo $projectLogo[$cntAA]->getImageTwo();?>" class="project-logo opacity-hover "></a>
                            <?php
                            }
                            else
                            {
                            ?>
                                <img src=" " class="project-logo opacity-hover ">
                            <?php
                            }
                        ?>

                    
                <?php
                }
                ?>
            <?php
            }
            ?>
				</div>
        </div>
    </div>

    <div class="clear"></div>
    
    <div class="width100 overflow margin-top30">
    	<div class="left-video-container-div overflow">
        <?php
        $conn = connDB();
        $subDetails = getSubShare($conn,"WHERE user_uid = ? AND status = 'Available' ", array("user_uid") ,array($referUidLink),"s");
        if($subDetails)
        {
            for($cntAA = 0;$cntAA < count($subDetails) ;$cntAA++)
            {
            ?>

                <?php 
                    $platfrom =  $subDetails[$cntAA]->getPlatform();
                    if($platfrom == 'Youtube')
                    {
                    ?>
                    
                        <div class="four-div overflow">
                            <iframe class="four-div-iframe" src="https://www.youtube.com/embed/<?php echo $subDetails[$cntAA]->getLink();?>?&playsinline=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            
                            <?php 
                                $extrafile = $subDetails[$cntAA]->getFile();
                                if($extrafile != "")
                                {
                                ?>
                                    <p class="gold-text four-div-p"><b><a href="" class="blue-to-orange" target="_blank">Live Tour</a> | <a href="uploadsFiles/<?php echo $subDetails[$cntAA]->getFile();?>" class="blue-to-orange" target="_blank">360</a></b></p>
                                <?php
                                }
                                else
                                {
                                ?>
                                <p class="gold-text four-div-p"><b><a href="" class="blue-to-orange" target="_blank">Live Tour</a></b></p>
                                <?php
                                }
                            ?>

                        </div>
                   
                    <?php
                    }
                    elseif($platfrom == 'Facebook')
                    {
                    ?>
                    
                        <div class="four-div overflow">
                            <iframe class="four-div-iframe" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F?v=<?php echo $subDetails[$cntAA]->getLink();?>?&playsinline=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <?php 
                                $extrafile = $subDetails[$cntAA]->getFile();
                                if($extrafile != "")
                                {
                                ?>
                                    <p class="gold-text four-div-p"><b><a href="" class="blue-to-orange" target="_blank">Live Tour</a> | <a href="uploadsFiles/<?php echo $subDetails[$cntAA]->getFile();?>" class="blue-to-orange" target="_blank">360</a></b></p>
                                <?php
                                }
                                else
                                {
                                ?>
                                <p class="gold-text four-div-p"><b><a href="" class="blue-to-orange" target="_blank">Live Tour</a></b></p>
                                <?php
                                }
                            ?>

                        </div>
                    
                    <?php
                    }
                    else
                    {}
                ?>

            <?php
            }
            ?>
        <?php
        }
        ?>
 		</div>
    
	<div class="left-video-container-div overflow">

		
        <?php
        $conn = connDB();
        $sharingDetails = getSharing($conn,"WHERE user_uid = ? AND status = 'Available' ", array("user_uid") ,array($referUidLink),"s");
        if($sharingDetails)
        {
            for($cntBB = 0;$cntBB < count($sharingDetails) ;$cntBB++)
            {
            ?>

                
                        
                        <!-- <div class="left-video-container-div overflow"> -->
                        
                            <div class="staff-div-css overflow opacity-hover">
                                <a href="<?php echo $sharingDetails[$cntBB]->getLink();?>" target="_blank">
                                    <!-- <div class="four-div-iframe staff-1"></div> -->
                                    <div class="four-div-iframe" id="<?php echo "style".$sharingDetails[$cntBB]->getId();?>"></div>
                                    <p class="gold-text four-div-p text-overflow"><b><?php echo $sharingDetails[$cntBB]->getRemark();?></b><br><?php echo $sharingDetails[$cntBB]->getHost();?></p>
                                </a>
                            </div>  
                        
                        <style>
                            /* .staff-1{ */
                            #<?php echo "style".$sharingDetails[$cntBB]->getId();?>
                            {
                                background-image:url("userProfilePic/<?php echo $sharingDetails[$cntBB]->getFile();?>");
                                background-size:cover;
                                background-position:top;
                            }
                        </style>
                  

            <?php
            }
            ?>
        <?php
        }
        ?>
		
    </div>
</div>
</div>

<?php include 'js.php'; ?>

</body>
</html>