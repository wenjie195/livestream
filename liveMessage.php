<?php
if (session_id() == "")
{
    session_start();
}
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$messageDetails = getMessage($conn," ORDER BY date_created ASC LIMIT 100");
// $messageDetails = getMessage($conn," WHERE id DESC LIMIT 50 ");

if($messageDetails)
{   
    for($cnt = 0;$cnt < count($messageDetails) ;$cnt++)
    {
		
    ?>

        <div class="chat-bubble-div">
           
            <p class="chat-bubble">
                <?php echo $messageDetails[$cnt]->getReceiveSMS();?>
            </p>
			<!-- <p class="username-date"><b>Username Here</b> 22/08/2020 10:00 AM</p> -->
            <p class="username-date"><b><?php echo $messageDetails[$cnt]->getUsername();?></b> <?php echo $messageDetails[$cnt]->getDateCreated();?></p>
        </div>

    <?php

    }
    ?>
			<script src="js/jquery-2.2.0.min.js" type="text/javascript"></script> 
		<script>
		 $('.chat-section').scrollTop($('.chat-section')[0].scrollHeight);	
		</script>	
<?php
}
$conn->close();
?>