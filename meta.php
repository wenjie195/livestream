
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width; initial-scale = 1.0; 
maximum-scale=1.0; user-scalable=no" /> 
<meta name="author" content="光明線上產業展 Guang Ming Virtual Expo Centre">
<meta property="og:image" content="https://gmvec.com/img/guangming-fb-meta.jpg" />
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-174229366-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-174229366-1');
</script>
