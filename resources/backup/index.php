<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Liveshare.php';
require_once dirname(__FILE__) . '/classes/Platform.php';
require_once dirname(__FILE__) . '/classes/Subshare.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();


// $liveDetails = getLiveShare($conn," WHERE username = 'MahSing' AND status = 'Available' AND type = '1' ");
$liveDetails = getUser($conn," WHERE broadcast_live = 'Available' AND user_type = '1' ");

$adminShare = getUser($conn," WHERE user_type = '0' ");
$adminData = $adminShare[0];
$adminPlatform = $adminData->getPlatform();
$adminLink = $adminData->getLink();
$adminAutoplay = $adminData->getAutoplay();
// $subDetails = getSubShare($conn," WHERE username = 'MahSing' AND status = 'Available' AND type = '1' ");
// $zoomDetails = getSubShare($conn," WHERE platform = 'Zoom' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Property" />
<title>Property</title>
<meta property="og:description" content="Property" />
<meta name="description" content="Property" />
<meta name="keywords" content="Livestream, Property, video, live, etc">

<?php include 'css.php'; ?>
</head>

<body>

<div class="width100 gold-line"></div>

<div class="width100 same-padding overflow gold-bg min-height">

    <div class="width100 overflow margin-top30">
    	<div class="width100 overflow text-center">
    		<img src="img/guangminglogo.png" class="guangming-logo" alt="Guang Ming Daily 光明日报" title="Guang Ming Daily 光明日报">
		</div>
        <h1 class="title-h1 text-center landing-title-h1 black-text">Guang Ming Merchant Event</h1>

        <?php
        if($adminAutoplay == "Yes")
        {
        ?>
            <div class="width100 overflow">
                <iframe class="landing-top-iframe" src="https://www.youtube.com/embed/<?php echo $adminLink;?>?&autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        <?php
        }
        else
        {
        ?>
            <div class="width100 overflow">
                <iframe class="landing-top-iframe" src="https://www.youtube.com/embed/<?php echo $adminLink;?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        <?php
        }
        ?>

        <div class="two-section-container overflow">
        <?php
        $conn = connDB();
        if($liveDetails)
        {
            for($cnt = 0;$cnt < count($liveDetails) ;$cnt++)
            {
            ?>

                <?php 
                    $platfrom =  $liveDetails[$cnt]->getPlatform();
                    if($platfrom == 'Youtube')
                    {
                    ?>
                        <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>' target="_blank">
                            <div class="two-section-div opacity-hover">
                                <p class="subtitle-p gold-text"><?php echo $liveDetails[$cnt]->getUsername();?></p> 
                                        <iframe class="two-section-iframe" src="https://www.youtube.com/embed/<?php echo $liveDetails[$cnt]->getLink();?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        <div class="clear"></div>
                                        <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>'><div class="guang-button">View More</div></a>
                            </div>
                        </a>
                    <?php
                    }

                    elseif($platfrom == 'Zoom')
                    {
                    ?>
                        <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>' target="_blank">
							<div class="two-section-div opacity-hover">
                            	<p class="subtitle-p gold-text"><?php echo $liveDetails[$cnt]->getUsername();?></p> 
                                <div class="two-section-iframe background-css" id="z<?php echo $liveDetails[$cnt]->getUid();?>" value="<?php echo $liveDetails[$cnt]->getUid();?>">
                            	</div>
                                <div class="clear"></div>
                                <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>'><div class="guang-button">View More</div></a>
							</div>
                        </a>
                        
						<style>
                        	#z<?php echo $liveDetails[$cnt]->getUid();?>{
								background-image:url(userProfilePic/<?php echo $liveDetails[$cnt]->getBroadcastShare();?>);}
                        </style>
                    <?php
                    }

                    elseif($platfrom == 'Facebook')
                    {
                    ?>

                        <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>' target="_blank">
							<div class="two-section-div opacity-hover">
                            	<p  class="subtitle-p gold-text"><?php echo $liveDetails[$cnt]->getUsername();?></p> 

   
                                    <iframe  class="two-section-iframe" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F?v=<?php echo $liveDetails[$cnt]->getLink();?>"  style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
                           			<div class="clear"></div>
                                    <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>'><div class="guang-button">View More</div></a>
                            </div>
                        </a>

                    <?php
                    }

                    else
                    {   }
                ?>

            <?php
            }
            ?>

        <?php
        }

        else
        {
        ?>
            NO BROADCASTING AT THE MOMENT, WE WILL RETURN SOON !!
        <?php
        }

        ?>
    </div>
    </div>

    <div class="clear"></div>
    
</div>

<?php include 'js.php'; ?>

</body>
</html>