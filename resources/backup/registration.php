<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://gmvec.com/registration.php" />
<meta property="og:title" content="Registration | 光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>Registration  | 光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="光明線上產業展, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live, etc">
<link rel="canonical" href="https://gmvec.com/registration.php" />
<?php include 'css.php'; ?>
</head>

<body class="body">

<div class="width100 same-padding overflow gold-bg min-height-footer-only">
	<h2 class="h1-title">Registration</h2> 
	<div class="clear"></div>
    <form action="utilities/registrationFunction.php" method="POST" class="margin-top30">
        <div class="dual-input">
            <p class="input-top-text">Username</p>
            <div class="password-input-div">
                <input class="aidex-input clean password-input"  type="text" placeholder="Username" id="register_username" name="register_username" required> 
            </div> 
        </div>  

        <div class="dual-input second-dual-input">
            <p class="input-top-text">Phone Number</p>
            <div class="password-input-div">
                <input class="aidex-input clean password-input"  type="text" placeholder="Phone Number" id="register_phone" name="register_phone" required>
            </div>  
        </div>  

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-text">Email</p>
            <div class="password-input-div">
                <input class="aidex-input clean password-input"  type="email" placeholder="Email" id="register_email" name="register_email" required> 
            </div> 
        </div> 

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-text">Password</p>
            <div class="password-input-div">
                <input class="aidex-input clean password-input"  type="password" placeholder="Password" id="register_password" name="register_password" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">  				
            </div>
        </div>
        
        <div class="dual-input second-dual-input">
            <p class="input-top-text">Retype Password</p>
            <div class="password-input-div">
                <input class="aidex-input clean password-input"  type="password" placeholder="Retype Password" id="register_retype_password" name="register_retype_password" required>  
                <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">
            </div>
        </div>

        <div class="clear"></div>

        <div class="width100 overflow text-center">     
            <button class="clean-button clean login-btn pink-button" name="submit">Sign Up</button>
        </div>
    </form>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>