<?php
if (session_id() == "")
{
    session_start();
}
// require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Message.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function sentMessage($conn,$message_uid,$receiveSMS)
{
     if(insertDynamicData($conn,"message",array("message_uid","receive_message"),
     array($message_uid,$receiveSMS),"ss") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $message_uid = md5(uniqid());
    $receiveSMS = rewrite($_POST["message_details"]);

    //   FOR DEBUGGING
    // echo "<br>";
    // echo $message_uid."<br>";
    // echo $receiveSMS."<br>";

    if(sentMessage($conn,$message_uid,$receiveSMS))
    {
        header('Location: ../liveChat.php?type=1');
    }
    else
    {
        header('Location: ../liveChat.php?type=2');
    }
}
else
{
     header('Location: ../index.php');
}
?>
